from urllib.parse import quote as url_encode
try : import ujson as json
except : import json
import aiohttp
import asyncio
import time

with open('credentials/bot.json') as credentials :
	credentials = json.load(credentials)
	token = credentials['telegramAccessToken']
	botid = credentials['telegramBotID']

acceptedMimeTypes = {
	'image/jpeg',
	'image/png',
	'image/gif'
}

ratingMap = {
	0: '⚪️',
	1: '🔵',
	2: '🔴',
}


responseFormat = '`>` [{} ⇗]({})'
separator = '\n\t'
def formatSources(sources) :
	return separator.join(responseFormat.format(s['artist'], s['source']) for s in sources)

async def handleImage(chatID, message, mime) :
	assert mime in acceptedMimeTypes

	request = f"https://api.telegram.org/bot{token}/getFile?file_id={message['file_id']}"
	async with aiohttp.request('GET', request) as response :
		response = json.loads(await response.text())
		if response['ok'] :
			imageurl = f"https://api.telegram.org/file/bot{token}/{response['result']['file_path']}"

	if imageurl :
		data = { 'url': imageurl }
		async with aiohttp.request('POST', 'https://api.kheina.com/v1/search', data=data) as response :
			data = json.loads(await response.text())

	if not data['error'] :
		results = (
			f"searched {data['stats']['images']:,} images from {data['stats']['artists']:,} artists in {data['elapsedtime']:0.2f} seconds\n\n" +
			'\n\n'.join(f"{r['similarity']:04.1f}% {ratingMap[r['sources'][0]['rating']]} {r['sources'][0]['title']}{separator}{formatSources(r['sources'])}" for r in data['results'][:10])
		)

		# send the message
		for _ in range(5) :
			try :
				async with aiohttp.request('GET', f'https://api.telegram.org/bot{token}/sendMessage?chat_id={chatID}&parse_mode=Markdown&text={url_encode(results)}') :
					return True
			except :
				pass

	else :
		# send an error
		return False


# functions go here
async def handleMessage(message) :
	# this function will call other handlers as necessary
	if 'photo' in message :
		await handleImage(message['chat']['id'], message['photo'][0], 'image/jpeg')
	elif 'document' in message :
		await handleImage(message['chat']['id'], message['document']['thumb'], message['document'].pop('mime_type'))
	else :
		print(message)


async def main() :
	request = f'https://api.telegram.org/bot{token}/getUpdates'
	mostrecentupdate = 0
	while True :
		response = None
		try :
			async with aiohttp.request('GET', f'{request}?offset={mostrecentupdate + 1}') as res :
				res = json.loads(await res.text())
				if res['ok'] :
					response = res['result']
		except KeyboardInterrupt :
			print('done.')
			exit(0)
		except Exception as e :
			print(f'{e.__class__.__name__}: {e}')

		if response :
			for message in response :
				# 'queue' the tasks for completion asynchronously
				asyncio.ensure_future(handleMessage(message['message']))
			mostrecentupdate = response[-1]['update_id']
		else :
			await asyncio.sleep(1)


if __name__ == '__main__' :
	asyncio.run(main())
